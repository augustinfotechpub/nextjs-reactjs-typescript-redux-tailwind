FROM node:alpine AS development

# Declaring env
#ENV NODE_ENV development

# Setting up the work directory
WORKDIR /

# Installing dependencies
COPY ./package.json /
RUN npm install

# Copying all the files in our project
COPY . .
EXPOSE  7774

# Starting our application
CMD [ "npm", "run"  ]

