import * as Yup from "yup";

const VALIDATION_SCHEMA = Yup.object().shape({
  name: Yup.string()
    .required("name is mandatory")
    .matches(/^(?! )[a-zA-Z ]+$/, "name must be of string Type"),

  email: Yup.string()
    .required("Email is mandatory")
    .email("Please enter a valid email."),

  password: Yup.string()
    .label("Password")
    .required("Password is mandatory")
    .min(8, "Try password atleast 8 char long")
    .max(12, "Try a password atmost 12 char long."),
});

export { VALIDATION_SCHEMA };
