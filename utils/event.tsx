import * as Yup from "yup";
import moment from "moment";

const EVENT_SCHEMA = Yup.object().shape({
  name: Yup.string().required("name is mandatory"),
endDate: Yup.string()
    .nullable()
    // .test("endDate", "Please select less date then 31",  (value) =>{
    //   return moment().diff(moment(value, "YYYY-MM-DD"), "years") >=31 ;
    // })
    .required("Please enter your end date"),
  description: Yup.string().required("description is mandatory"),
  location:Yup.string().required("location is mandatory")
});

export { EVENT_SCHEMA };
