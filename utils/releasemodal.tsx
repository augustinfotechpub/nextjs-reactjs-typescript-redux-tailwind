import * as Yup from "yup";

const VALIDATION_SCHEMA = Yup.object().shape({
  title: Yup.string().required("Title is mandatory"),
  url: Yup.string().required("Url is mandatory"),
  date: Yup.string().required("date is mandatory"),
});



const MODAL_USER = Yup.object().shape({
  name: Yup.string().required("name is mandatory"),
  email:Yup.string().email().min(5).max(30).required("email is mandatory"),
  password:Yup.string()
    .required("Password is mandatory")
    .min(8, "Try password atleast 8 char long")
    .max(12, "Try a password atmost 12 char long."),
});

export { VALIDATION_SCHEMA ,MODAL_USER};
