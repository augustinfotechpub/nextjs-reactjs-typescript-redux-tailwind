import React, { useContext, useEffect, useRef, useState } from 'react';
import type { InputRef } from 'antd';
import { Button, Form, Input, Popconfirm, Table } from 'antd';
import type { FormInstance } from 'antd/es/form';
import {Modal } from "antd";
import useGet from 'hooks/useGet';
import router, { useRouter } from 'next/router';
import usePost from 'hooks/usePost';
import { MODAL_USER } from 'utils/releasemodal';

const EditableContext = React.createContext<FormInstance<any> | null>(null);

interface Item {
  key: string;
  name: string;
  age: string;
  address: string;
}

interface EditableRowProps {
  index: number;
}

const EditableRow: React.FC<EditableRowProps> = ({ index, ...props }) => {

  const [form] = Form.useForm();
  const router = useRouter();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface EditableCellProps {
  title: React.ReactNode;
  editable: boolean;
  children: React.ReactNode;
  dataIndex: keyof Item;
  record: Item;
  handleSave: (record: Item) => void;
}

const EditableCell: React.FC<EditableCellProps> = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef<InputRef>(null);
  const form = useContext(EditableContext)!;

  useEffect(() => {
    if (editing) {
      inputRef.current!.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({ [dataIndex]: record[dataIndex] });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();

      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{ margin: 0 }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div className="editable-cell-value-wrap" style={{ paddingRight: 24 }} onClick={toggleEdit}>
        {children}
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};

type EditableTableProps = Parameters<typeof Table>[0];

interface DataType {
  key: React.Key;
  name: string;
  email: string;
  createdAt: string;
}

type ColumnTypes = Exclude<EditableTableProps['columns'], undefined>;

const ReleaseTable: React.FC = () => {
  const [form] = Form.useForm();
  const [dataSource, setDataSource] = useState<DataType[]>([

  ]);

  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

  const [count, setCount] = useState(2);
  

  const handleDelete = (key: React.Key,record:any) => {

    const newData = dataSource.filter((item) => {
 
    item.key !== key
    });
 
   
      onDelete(record.email)
    setDataSource(newData);
  };

  const defaultColumns: (ColumnTypes[number] & { editable?: boolean; dataIndex: string })[] = [
    {
      title: 'name',
      dataIndex: 'name',
      width: '30%',
      editable: true,
    },
    {
      title: 'email',
      dataIndex: 'email',
      editable: true,
    },
    {
      title: 'creadtedAt',
      dataIndex: 'createdAt',
     
    },
    {
      title: 'operation',
      dataIndex: 'operation',
      render: (_, record: { key: React.Key }) =>
        dataSource.length >= 1 ? (
          <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.key,record)}>
            <a>Delete</a>
          </Popconfirm>
        ) : null,
    },
  ];

  const handleAdd = () => {
    setIsModalOpen(true)
  }
  const {
    data: viewData,
    refetch: getViewDataRefetch,
    isLoading: dataLoading,
    isError,
    error,
  } = useGet('getViewData', 'api/users/getAll', false, {
    enabled: false,
    token: true,
  })

 
    
 
  
    useEffect(() => {

 
  }, [dataSource,getViewDataRefetch])

  const handleSave = (row: DataType) => {
    const newData = [...dataSource];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    setDataSource(newData);
  };

  const components = {
    body: {
      row: EditableRow,
      cell: EditableCell,
    },
  };

  const columns = defaultColumns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record: DataType) => ({
        record,
        editable: col.editable,
        dataIndex: col.dataIndex,
        title: col.title,
        handleSave,
      }),
    };
  });

 
  const handleCancel = () => {    
    setIsModalOpen(!isModalOpen);
    form.resetFields()
  }


  const { mutateAsync } = usePost();

  

  const onFinish = async (values: any) => {
    try {
     
    const date= Date.now()
    if (isModalOpen) {
    const newData: DataType = {
      key: count,
      name: `${values.name}`,
      email: `${values.email}`,
      createdAt: `${date}`,
    };
    setDataSource([...dataSource, newData]);
    setCount(count + 1);
    }
     setIsModalOpen(!isModalOpen);
      const payload={      
        name:values.name,
      email: values.email,
      password: values.password,}
        form.resetFields()
        const response = await mutateAsync({
          url: "/api/users/register",
          payload:payload
        });
     
        if (response.status === 201) {
     router.push("/dashboard");
       
        }
      } catch (error: any) {
        return { error: error?.response?.data?.errorMessage };
      }
   
  };
    const yupSync = {
  async validator({ field}:any, value:any) {
    await MODAL_USER.validateSyncAt(field, { [field]: value });
  },
  };
   useEffect(() => {
    if (viewData) {
       
    setDataSource(viewData.User)
    }
 
    getViewDataRefetch()
   }, [!isModalOpen])
     useEffect(() => {
    if (viewData) {
       
    setDataSource(viewData.User)
    }
 
    getViewDataRefetch()
   }, [viewData])
 const { mutateAsync: mutateAsyncDelete } = usePost();
  const onDelete = async (item: any) => {
     
    try {
      const payload = { email: `${item}` }
      const response = await mutateAsyncDelete({
        url: "/api/users/remove",
        payload: payload
      });
      if (response.status === 200) {
        router.push("/dashboard");
       location.reload()
      }
      else { router.push("/dashboard"); }
    }
    catch (error: any) {
   
    (error);
 
    }
  }

  return (

    <div>

      <Button onClick={handleAdd} type="primary" style={{ marginBottom: 16 }}>
        Add a row
      </Button>
      <Table
        components={components}
        rowClassName={() => 'editable-row'}
        bordered
        dataSource={dataSource}
        columns={columns as ColumnTypes}
      />
  <Modal  title="Basic Modal" open={isModalOpen} onCancel={handleCancel}  footer={null}>
        <Form
          form={form}
       onFinish={onFinish}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 14 }}
        initialValues={{ remember: true }}
      >
       
        <Form.Item label=" Name"
        name="name"
        
rules={[yupSync]}
        >
          <Input />
        </Form.Item>
        <Form.Item label=" email"
            name="email"
              rules={[yupSync]}>
          <Input type="email"  />
        </Form.Item>
        
       
      
              <Form.Item label=" password"
              name="password"
   
     rules={[yupSync]}>
          <Input />
        </Form.Item>
        
       
   <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
      </Form>
       
      </Modal>
    </div>
  );
};

export default ReleaseTable;

