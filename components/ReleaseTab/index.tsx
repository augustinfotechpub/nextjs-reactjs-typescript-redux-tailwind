import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Modal from "react-modal";
import Button from "components/Button";
import ReleaseTable from "components/ReleaseTab/releaseTable";
import ReleaseDetails from "components/ReleaseDetails";
import AddReleaseModal from "components/ReleaseTab/AddReleaseModal";
import { fetchReleases } from "store/releasesSlice";
import {
  ButtonWrapper,
  TableWrapper,
  MainWrapper,
  Wrapper,
} from "styles/components/ReleaseTab";

interface ReleaseTabsProps {
  active?: string;
}

export const ReleaseTab = ({ active }: ReleaseTabsProps) => {
  const [details, setDetails] = useState(false);
  const dispatch = useDispatch();

  const handleDetails = () => {
    setDetails(true);
  };
  const [modalIsOpen, setIsOpen] = useState(false);

  useEffect(() => {
    dispatch(fetchReleases());
  }, []);

  return (
    <MainWrapper>
      <div className="w-full">
        {details ? (
          <ReleaseDetails />
        ) : (
          <>
            <Wrapper>
              <h1 className="block uppercase text-blueGray-600 text-xl font-bold mb-2">
                Users
              </h1>

              <Modal
                isOpen={modalIsOpen}
                onRequestClose={() => setIsOpen(false)}
                contentLabel="Example Modal"
              >
                <AddReleaseModal
                  showModal={(value: boolean) => setIsOpen(value)}
                />
              </Modal>
            </Wrapper>
            <TableWrapper>
              <ReleaseTable act={active} />
            </TableWrapper>
          </>
        )}
      </div>
    </MainWrapper>
  );
};
