import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { LoadingOutlined } from "@ant-design/icons";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import useFormPost from "hooks/useFormPost";
import { DatePicker, Space, notification } from "antd";
import Button from "components/Button";
import { fetchReleases } from "store/releasesSlice";
import { VALIDATION_SCHEMA } from "utils/releasemodal";
import {
  FormWrapper,
  FormContainer,
  FormHeader,
  InputContainer,
  CloseModalButton,
  HeaderContainer,
  DateContent,
  InputTextareaContainer,
} from "styles/components/ReleaseTab";
import { ModalProps } from "interface";

type NotificationType = "success" | "info" | "warning" | "error";

const AddReleaseModal = ({ showModal }: ModalProps) => {
  const [api, contextHolder] = notification.useNotification();
  const { mutateAsync } = useFormPost();
  const [disable, setDisable] = useState(false);
  const dispatch = useDispatch();

  const openNotificationWithIcon = (
    type: NotificationType,
    message: string,
    description: string
  ) => {
    api[type]({
      message: message,
      description: description,
    });
  };
  const {
    handleSubmit,
    register,
    control,
    formState: { errors },
  } = useForm({
    mode: "onSubmit",
    reValidateMode: "onChange",
    resolver: yupResolver(VALIDATION_SCHEMA),
    defaultValues: {
      title: "",
      description: "",
      url: "",
      date: "",
    },
  });

  const submitData = async (values: any) => {
    setDisable(true);
    const payload = {
      ...values,
      projectId: window.localStorage.getItem("projectId"),
    };
    try {
      openNotificationWithIcon(
        "info",
        "Release is being created",
        "just wait for few seconds..."
      );
      const response = await mutateAsync({
        url: "/api/releases/create",
        payload: payload,
      });
      if (response.status === 201) {
        dispatch(fetchReleases());
        openNotificationWithIcon(
          "success",
          "Release created",
          "Congratulations..."
        );
        setTimeout(() => {
          showModal(false);
          setDisable(false);
        }, 500);
      }
    } catch (error: any) {
      setDisable(false);
      openNotificationWithIcon(
        "error",
        `${error?.response?.data?.msg}`,
        "Something went wrong, Kindly try again later..."
      );
      return { error: error?.response?.data?.errorMessage };
    }
  };

  const date = new Date();

  return (
    <FormWrapper>
      <FormContainer onSubmit={handleSubmit(submitData)}>
        {contextHolder}
        <HeaderContainer>
          <FormHeader>Add Releases</FormHeader>
          <CloseModalButton onClick={() => showModal(false)}>
            X
          </CloseModalButton>
        </HeaderContainer>
        <InputContainer>
          <label className="block text-blueGray-600 text-xs font-bold mb-1">
            Title
          </label>
          <input
            type="text"
            className={` py-2 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm  focus:outline-none focus:ring w-full ease-linear transition-all duration-15  ${
              errors.title ? "focus:ring-red-400" : "focus:ring-blue-500"
            } `}
            placeholder="Title"
            {...register("title")}
          />
          {errors.title ? (
            <span className="text-rose-500 text-left text-sm mt-1 mb-1">
              {errors?.title?.message}
            </span>
          ) : (
            <span className="h-4"></span>
          )}
        </InputContainer>
        <InputContainer>
          <label className="block  text-blueGray-600 text-xs font-bold mb-1">
            URL
          </label>
          <input
            type="text"
            className={`py-2 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm  focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ${
              errors.url ? "focus:ring-red-400" : "focus:ring-blue-500"
            }`}
            placeholder="Url"
            {...register("url")}
          />
          {errors.url ? (
            <span className="text-rose-500 text-left text-sm mt-1 mb-1">
              {errors?.url?.message}
            </span>
          ) : (
            <span className="h-4"></span>
          )}
        </InputContainer>
        <InputContainer>
          <label className="block text-blueGray-600 text-xs font-bold mb-1">
            Released Date
          </label>
          <DateContent>
            <Space direction="vertical">
              <Controller
                control={control}
                name="date"
                render={({ field: { onChange } }) => (
                  <DatePicker
                    onChange={onChange}
                    disabledDate={(d) => !d || d.isBefore(date)}
                  />
                )}
              />
            </Space>
            {errors.date ? (
              <span className="text-rose-500 text-left text-sm mt-1 mb-2">
                {errors?.date?.message}
              </span>
            ) : (
              <span className="h-4"></span>
            )}
          </DateContent>
        </InputContainer>
        <InputTextareaContainer>
          <label className="block  text-blueGray-600 text-xs font-bold mb-1">
            Description
          </label>
          <textarea
            rows={6}
            className={`py-2 px-2 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm  focus:outline-none focus:ring w-full ease-linear transition-all duration-150 resize-none ${
              errors.description ? "focus:ring-red-400" : "focus:ring-blue-500"
            }`}
            placeholder="Description"
            {...register("description")}
          ></textarea>
          {errors.description ? (
            <span className="text-rose-500 text-left text-sm mt-1 mb-1">
              {errors?.description?.message}
            </span>
          ) : (
            <span className="h-4"></span>
          )}
        </InputTextareaContainer>
        <div className="text-center flex ">
          <Button
            variant="contained"
            disable={disable}
            type="submit"
            label={
              disable ? (
                <LoadingOutlined spin style={{ fontSize: "20px" }} />
              ) : (
                "Add"
              )
            }
          />
        </div>
      </FormContainer>
    </FormWrapper>
  );
};

export default AddReleaseModal;
