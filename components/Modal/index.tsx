import React from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { yupResolver } from "@hookform/resolvers/yup";
import { notification, Select } from "antd";
import { createProjectData } from "data/data";
import Button from "components/Button";
import { VALIDATION_SCHEMA } from "utils/createProjectValidation";
import { fetchProjects } from "store/projectSlice";
import usePost from "hooks/usePost";
import modalbg from "assets/Images/modalbg.png";
import {
  ModalRightSide,
  ModalImage,
  CloseModalButton,
  ModalRightContent,
  CustomStyle,
} from "styles/pages/Create";
import {
  ButtonWrapper,
  Description,
  ElementDescription,
  ElementTitle,
  ElementWrapper,
  Input,
  LeftContainer,
  LeftContainerWrapper,
  Optional,
  TextArea,
  Title,
} from "styles/Workspace";
import { ModalProps } from "interface";

type NotificationType = "success" | "info" | "warning" | "error";

const CreateProject = ({ showModal }: ModalProps) => {
  const dispatch = useDispatch();

  const { mutateAsync } = usePost();
  const [api, contextHolder] = notification.useNotification();

  const openNotificationWithIcon = (
    type: NotificationType,
    message: string,
    description: string
  ) => {
    api[type]({
      message: message,
      description: description,
    });
  };
  const {
    handleSubmit,
    register,
    control,
    formState: { errors },
  } = useForm({
    mode: "onSubmit",
    reValidateMode: "onChange",
    resolver: yupResolver(VALIDATION_SCHEMA),
    defaultValues: {
      name: "",
      description: "",
      projectType: "",
    },
  });

  const submitData = async (values: any) => {
    openNotificationWithIcon(
      "info",
      "Your Project is getting created",
      "You are just few moments away from managing your project smoothely..."
    );
    const payload = {
      projectData: values,
    };

    try {
      const response = await mutateAsync({
        url: "/api/projects/create",
        payload: payload,
      });
      if (response.status === 201) {
        dispatch(fetchProjects());
        openNotificationWithIcon(
          "success",
          `Project created successflly`,
          "Kindly add members and start living without stress... "
        );
        setTimeout(() => {
          showModal(false);
        }, 1000);
      }
    } catch (error: any) {
      openNotificationWithIcon(
        "error",
        `${error?.response?.data?.errors?.[0]?.msg}`,
        "Something went wrong, Kindly try again later..."
      );
      return { error: error?.response?.data?.errorMessage };
    }
  };

  const selectStyle = {
    width: "100%",
    borderRadius: "5px",
    height: 40,
    border: "2px solid gray",
  };

  return (
    <CustomStyle>
      {contextHolder}
      <LeftContainer>
        <LeftContainerWrapper>
          <Title>Let's build a Workspace</Title>
          <Description>
            Boost your productivity by making it easiar for everyone to access
            board in one location.
          </Description>
          <form onSubmit={handleSubmit(submitData)}>
            <ElementWrapper>
              <ElementTitle>Workspace name</ElementTitle>
              <Input
                placeholder="Taka's co"
                {...register("name")}
                className={`focus:border-0 focus:outline-none focus:ring  ${
                  errors.name ? "focus:ring-red-400" : "focus:ring-blue-500"
                }`}
              />
              {errors.name ? (
                <span className="text-rose-500 text-left text-sm">
                  {errors?.name?.message}
                </span>
              ) : (
                <span className="h-1"></span>
              )}
              <ElementDescription>
                This is the name of your company, team or organization.
              </ElementDescription>
            </ElementWrapper>
            <ElementWrapper>
              <ElementTitle>Workspace Type</ElementTitle>

              <Controller
                control={control}
                name="projectType"
                render={({ field: { onChange, onBlur } }) => (
                  <Select
                    allowClear
                    options={createProjectData}
                    onChange={onChange}
                    onBlur={onBlur}
                    placeholder="Choose.."
                    style={selectStyle}
                    bordered={false}
                  />
                )}
              />
              {errors.name ? (
                <span className="text-rose-500 text-left text-sm">
                  {errors?.projectType?.message}
                </span>
              ) : (
                <span className="h-1"></span>
              )}
            </ElementWrapper>

            <ElementWrapper>
              <ElementTitle>
                Workspace description <Optional>(Optional)</Optional>
              </ElementTitle>
              <TextArea
                placeholder="Our team organize everything here."
                {...register("description")}
              />
              <ElementDescription>
                Get your members on board with a few words about your Workspace.
              </ElementDescription>
            </ElementWrapper>
            <ButtonWrapper>
              <Button type="submit" variant="contained" label="Continue" />
            </ButtonWrapper>
          </form>
        </LeftContainerWrapper>
      </LeftContainer>
      <ModalRightSide>
        <CloseModalButton onClick={() => showModal(false)}>X</CloseModalButton>
        <ModalRightContent>
          <ModalImage src={modalbg.src} alt="ModalBG" className="img" />
        </ModalRightContent>
      </ModalRightSide>
    </CustomStyle>
  );
};

export default CreateProject;
