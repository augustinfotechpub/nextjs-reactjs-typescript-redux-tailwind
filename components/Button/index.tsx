import React, { ReactElement } from "react";
import { StyledButton } from "styles/Button";

export interface IButtonProps {
  label: ReactElement | string;
  disable?: boolean;
  variant?: "contained" | "text" | "outline";
  type?: "button" | "submit" | "reset";
  onClick?: () => void;
}

const Button: React.FC<IButtonProps> = ({
  label,
  variant,
  type,
  onClick,
  disable,
  ...rest
}: IButtonProps) => (
  <StyledButton
    variant={variant}
    {...rest}
    type={type}
    onClick={onClick}
    disabled={disable}
  >
    {label}
  </StyledButton>
);

Button.defaultProps = {
  variant: "contained",
  onClick: () => null,
};

export default Button;
