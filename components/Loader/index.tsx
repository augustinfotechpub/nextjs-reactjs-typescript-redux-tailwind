import React from "react";
import { LoadingOutlined } from "@ant-design/icons";
import { Spin } from "antd";
import { LoaderWrapper } from "styles/components/Loader";

const antIcon = <LoadingOutlined style={{ fontSize: 45 }} spin />;

const Loader = () => {
  return (
    <LoaderWrapper>
      <Spin size="large" />
    </LoaderWrapper>
  );
};

export default Loader;
