import React from "react";
import { Carousel } from "antd";
import Breadcrumb from "components/Breadcrumb";
import releaseimg from "assets/Images/land.jpg";
import carimg from "assets/Images/projectBackground.jpeg";
import {
  DetailedContainer,
  DetailedContent,
  DetailedWrapper,
  Heading,
  ImageContainer,
  InnerContainer,
  LinkContainer,
} from "styles/components/ReleaseDetails";

const ReleaseDetails = () => {
  const images = [{ url: releaseimg.src }, { url: carimg.src }];
  return (
    <DetailedContainer>
      <Breadcrumb itemRender="Details" />
      <DetailedWrapper>
        <Heading>Title</Heading>
        <LinkContainer>url: https://www.xyz.org</LinkContainer>
        <ImageContainer>
          <Carousel autoplay>
            {images.map((image) => (
              <img src={image.url} alt="" />
            ))}
          </Carousel>
        </ImageContainer>
        <InnerContainer>
          <DetailedContent>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi nisi
            minus placeat mollitia nesciunt, tempore atque delectus vel, dolore
            reprehenderit doloribus illo similique velit quibusdam illum
            cupiditate ut. Dolor, quas? Lorem ipsum dolor sit amet consectetur
            adipisicing elit. Veritatis illo, id voluptas reiciendis earum
            beatae repudiandae voluptatum fugiat autem? Optio libero officia
            odit dolores veritatis molestias aut eaque, fugiat consequatur.
            Lorem ipsum dolor, sit amet consectetur adipisicing elit.
            Perspiciatis molestias sunt deserunt totam vel voluptates rem quasi
            esse, laboriosam distinctio ex quo excepturi quos fugit aperiam,
            aspernatur voluptate quisquam nihil?
          </DetailedContent>
        </InnerContainer>
      </DetailedWrapper>
    </DetailedContainer>
  );
};

export default ReleaseDetails;
