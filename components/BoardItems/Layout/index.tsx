import React from "react";
import TopBar from "components/BoardItems/Topbar";
import SideBar from "components/BoardItems/Sidebar";

const  Layout=({ children }: any)=> {
  return (
    <div className="min-w-full min-h-screen  h-screen overflow-hidden bg-[#eaf4fb80]">
      <TopBar />
      <SideBar />
    </div>
  );
}

export default Layout;
