import React, { useState } from "react";
import { useRouter } from "next/router";
import { Tabs } from "antd";
import {
  FormOutlined,
  UserAddOutlined,
} from "@ant-design/icons";
import { ReleaseTab } from "components/ReleaseTab";

import { TabContainer } from "styles/components/Sidebar";
import Event from "components/Event";

const SideBar = () => {
  const router = useRouter();
  const [active, setActive] = useState<string>();
  const goToHome = () => {
    router.push("/dashboard");
  };
  return (
    <>
      <TabContainer >
        <div className=" inset-y-0 w-36 left-0 bg-white ml-4 rounded-tr-2xl rounded-tl-2xl mt-4 sticky shadow-xl">
          <h1
            className="flex items-center justify-center text-2xl h-20 text-[#65676d] font-bold cursor-pointer"
            onClick={goToHome}
          >
            Proof of Concept
          </h1>
        </div>
        <Tabs
          defaultActiveKey="1"
          tabPosition="left"
          className="flex h-full ml-4 sticky shadow-xl"
          onChange={(e: string) => {
            setActive(e);
         
          }}
          items={[
           
            {
              label: (
                <li
                  className="flex justify-center items-center flex-row
           py-5 text-gray-500"
                >
                  <UserAddOutlined />

                  <p className="text-sm mb-0"> Users</p>
                </li>
              ),
              key: "1",
              children: <ReleaseTab active={active} />,
            },
            
           
            {
              label: (
                <li
                  className="flex justify-center  items-center 
                  py-5 text-gray-500"
                >
                  <FormOutlined />

                  <p className="text-sm mb-0"> Calendar </p>
                </li>
              ),
              key: "2",
              children: <Event />,
            },
          ]}
        />
      </TabContainer>
    </>
  );
};

export default SideBar;
