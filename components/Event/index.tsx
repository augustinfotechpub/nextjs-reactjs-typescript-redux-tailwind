import {  useEffect, useState } from "react";

import type { Dayjs } from 'dayjs';
import { Badge, BadgeProps, Button, Calendar, DatePicker, Form, Input, Modal } from "antd";
import BreadcrumbTab from "components/Breadcrumb";

import usePost from "hooks/usePost";
import { useRouter } from "next/router";
import { EVENT_SCHEMA } from "utils/event";
import useGet from "hooks/useGet";

const { TextArea } = Input;


const Event = () => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [sDate, setSDate] = useState(" ");
  const [eDate, setEDate] = useState(" ");
  const { mutateAsync } = usePost();
  const router = useRouter();
  const [form] = Form.useForm();
  const [apiData , setApiData] = useState([])
  const [eventData, setEventData] = useState<any>([

    {
   startDate: "2002-03-08",
   endDate: "2002-03-09",
   name: "asdasd",
   location: "string",
   description: "string",
   dates: ["2023-01-08", "2023-01-09"]
  },
  {
    startDate: "2002-03-08",
    endDate: "2002-03-09",
    name: "asdasd",
    location: "string",
    description: "string",
    dates: ["2023-01-09", "2023-01-09"]
   },
  {
    name: "test",
   startDate: "2002-03-07",
   endDate: "2002-03-07",
   location: "sdvfdvfd",
   description: "sdvsvsdv",
   dates: ["2023-01-07"]
  }
  ,
  {
    name: "pun",
   startDate: "2002-03-14",
   endDate: "2002-03-16",
   location: "sdvfdvfd",
   description: "sdvsvsdv",
   dates: ["2023-01-14", "2023-01-15", "2023-01-16"]
  }
  ,
  {
    name: "dun",
   startDate: "2002-03-30",
   endDate: "2002-04-01",
   location: "sdvfdvfd",
   description: "sdvsvsdv", 
   dates: ["2023-01-30", "2023-02-01", "2023-01-31"]
  }
  ,
  ]); 



  const {
    data: viewEvents,
    refetch: getViewEventsRefetch,
    isLoading: dataLoading,
    isError,
    error,
  } = useGet('getEventsData', 'api/events/getAll', false, {
    enabled: false,
    token: true,
  })


  useEffect(() => {
    getViewEventsRefetch();
    
  if(viewEvents != undefined){
    setEventData(viewEvents.event);
    
  }
  }, [eventData , viewEvents])

  useEffect(() => {
     
      }, [eventData])

  
  const onSelect = (value : Dayjs) => {
    
    setIsModalOpen(!isModalOpen);
    setSDate(value.format("YYYY-MM-DD"));
  }

  const handleCancel = () => {    
     
    setIsModalOpen(!isModalOpen);
    form.resetFields()
  }

  
  const dateCellRender = (value: Dayjs) => {
    const stringValue = value.format("YYYY-MM-DD");
    const listData = eventData.filter((event : any)=>{
      if(event.dates.includes(stringValue)){
        return event
      }
    })
    return (
      <ul className="events">
        {listData.map((item : any) => (
          <li key={item.name}>
                        <Badge status={'success' as BadgeProps['status']} text={item.name} />
                        <br/>

          </li>
        ))}
      </ul>
      
    );
  };


  const onFinish = async(values: any) => {
       try {
        const dates =[]
        dates.push(sDate)
        setEDate(values.endDate.format("YYYY-MM-DD"))
        
        const start = sDate.split("-");
        const end  = values.endDate.format("YYYY-MM-DD").split("-");

       const duration : number = (+end[0] - +start[0])*365 + (+end[1] - +start[1])*30 + (+end[2] - +start[2]) ;
       

       for(let i = 1 ; i<= duration ;++i){
        let newDay = +start[2] + i
        let newMonth = +start[1]
        let newYear = +start[0]
        if(newDay > 31) {
           newMonth = +start[1] + 1
        }
        if(newMonth > 12){
           newYear = +start[0] + 1
        } 
        let NEWDAY = ''
        let NEWMONTH = ''

        if(newDay<10){
          newDay.toString()
           NEWDAY = "0" + newDay
        }else{
          NEWDAY = newDay.toString()
        }

        if(newMonth<10){
          newMonth.toString()
           NEWMONTH = "0" + newMonth
        }else{
          NEWMONTH = newMonth.toString()
        }

        const newDate = newYear +"-"+ NEWMONTH +"-" + NEWDAY;
        dates.push(newDate);
       }
       form.resetFields();
       setIsModalOpen(!isModalOpen)
      const payload= {
        name: values.name,
        startDate: sDate,
        endDate: values.endDate.format("YYYY-MM-DD"),
        location: values.location,
        description: values.description,
        dates : dates
      }
      console.log(payload);
      
        const response = await mutateAsync({
          url: "/api/events/create",
          payload : payload,
        });
        if (response.status === 200) {
      router.push("/dashboard");
          location.reload()
        }
      
      } catch (error: any) {
        return { error: error?.response?.data?.errorMessage };
      }
  };


 const yupSync = {
  async validator({ field}:any, value:any) {
    await EVENT_SCHEMA.validateSyncAt(field, { [field]: value });
  },
};



  return (
      <><BreadcrumbTab itemRender="Events" />

      <div style={{display: "flex", minWidth : "100%" , alignItems : "center", justifyContent : "center" }}>
        
      <Calendar dateCellRender={dateCellRender} fullscreen = {true} onSelect={onSelect}/>
      
      </div>
    
      
        <Modal title="Add Events" open={isModalOpen} onCancel={handleCancel} footer={null}>

        <Form form = {form}
       onFinish={onFinish}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 14 }}
        initialValues={{ remember: true }}
      >
       
        <Form.Item label="Event Name"
        name="name"
         rules={[yupSync]}
        >
          <Input />
        </Form.Item>
        <Form.Item label="Location"
        name="location"
       rules={[yupSync]}>
          <Input />
        </Form.Item>
        <Form.Item label="pick End Date"
        name="endDate"
         rules={[yupSync]}>
          <DatePicker />
        </Form.Item>
        
        <Form.Item label="Description"
        name="description"
         rules={[yupSync]}>
          <TextArea rows={4} />
        </Form.Item>
       
        <Form.Item>
        <Button type="primary" htmlType="submit"  >
          Submit
        </Button>
      </Form.Item>
      </Form>
      </Modal>
</>
  );
};

export default Event;


