import * as React from "react";

const EqualIcon = () => (
  <svg
    width="12"
    height="12"
    fill="#ffab00"
    viewBox="0 0 700 700"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <path
      d="M81.953 84.297h536.1c46.098 0 81.953 37.562 81.953 81.953 0 46.098-35.855 81.953-81.953 81.953h-536.1C35.855 248.203 0 212.348 0 166.25c0-44.391 37.562-81.953 81.953-81.953zM81.953 345.52h536.1c46.098 0 81.953 37.562 81.953 81.953 0 46.098-35.855 81.953-81.953 81.953h-536.1C35.855 509.426 0 473.571 0 427.473c0-44.391 37.562-81.953 81.953-81.953z"
      fillRule="evenodd"
    />
  </svg>
);

export default EqualIcon;
