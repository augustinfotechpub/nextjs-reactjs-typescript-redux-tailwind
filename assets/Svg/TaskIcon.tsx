import * as React from "react";
import { SVGProps } from "react";

const SvgComponent = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={16}
    height={16}
    viewBox="0 0 16 16"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <title>{"task"}</title>
    <g transform="translate(1 1)" fill="none" fillRule="evenodd">
      <rect fill="#4BADE8" width={14} height={14} rx={2} />
      <g stroke="#FFF" strokeWidth={2} strokeLinecap="round">
        <path d="m6 9.5 4-5M6 9.5l-2-2" />
      </g>
    </g>
  </svg>
);

export default SvgComponent;
