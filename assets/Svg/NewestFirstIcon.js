import * as React from "react";

const NewestFirstIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={18}
    height={15}
    viewBox="0 0 512 512"
    transform="rotate(180) scale(-1 1)"
  >
    <path d="M108.7 95.8 51 153.5l18.3 18.3L87.5 190l26-26c14.3-14.3 26.3-26 26.7-26 .5 0 .8 75.6.8 168v168h52V137.5l26.3 26.3 26.2 26.2 18.5-18.5 18.5-18.5L225 95.5C193.4 63.9 167.3 38 167 38c-.3 0-26.5 26-58.3 57.8z" />
    <path d="M238 239v26h111v-52H238v26zM238 344v26h149v-52H238v26zM238 448v26h223v-52H238v26z" />
  </svg>
);

export default NewestFirstIcon;
