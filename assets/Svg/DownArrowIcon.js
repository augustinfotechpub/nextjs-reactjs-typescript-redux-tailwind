import * as React from "react";

const DownArrowIcon = () => (
  <svg
    width="22"
    height="22"
    fill="white"
    viewBox="0 0 700 700"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M521.58 211.41c4.555-4.555 4.555-11.941 0-16.5l-16.5-16.496c-4.555-4.559-11.945-4.559-16.5 0l-138.59 138.59-138.58-138.59c-4.555-4.559-11.945-4.559-16.5 0l-16.5 16.496c-4.555 4.559-4.555 11.945 0 16.5l163.34 163.33c4.555 4.559 11.941 4.559 16.5 0z"
      fillRule="evenodd"
    />
  </svg>
);

export default DownArrowIcon;
