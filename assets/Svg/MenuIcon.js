import * as React from "react";

const MenuIcon = () => (
  <svg
    width="22"
    height="22"
    transform="rotate(90)"
    viewBox="0 0 700 700"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M350 385c-38.641 0-70 31.359-70 70s31.359 70 70 70 70-31.359 70-70-31.359-70-70-70zm0-175c-38.641 0-70 31.359-70 70s31.359 70 70 70 70-31.359 70-70-31.359-70-70-70zm0-175c-38.641 0-70 31.359-70 70s31.359 70 70 70 70-31.359 70-70-31.359-70-70-70z"
      fillRule="evenodd"
    />
  </svg>
);

export default MenuIcon;
