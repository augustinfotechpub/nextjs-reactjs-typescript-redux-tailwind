import { useMutation } from "react-query";
import workForceInstance from "services/axiosInstance";

interface IParam {
  url: string;
  payload?: any;
  token?: string;
  formData?: string;
}
const post = async ({ url, payload, formData }: IParam) => {
  const authToken = localStorage.getItem("token");

  const data = await workForceInstance.post(url, payload, {
    headers: { token: authToken, "Content-Type": formData },
  });
  return data;
};

const useFormPost = () => useMutation(post);

export default useFormPost;
