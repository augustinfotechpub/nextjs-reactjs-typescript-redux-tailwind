import { useMutation } from "react-query";
import workForceInstance from "services/axiosInstance";

interface IParam {
  url: string;
  payload?: any;
  token?: string;
}
const del = async ({ url, payload }: IParam) => {
    const authToken = localStorage.getItem("token");
     const headers = { token: authToken }

   const { data } = await workForceInstance.delete(url, {
    headers,
    data: payload,
  })
     
  return data;
};

const useDelete = () => useMutation(del);

export default useDelete;
