import { ReactElement } from "react";

export interface MemberInterface {
  avatar: ReactElement;
  name: string;
  _id: string;
  ticket: string[];
  email: string;
  phone: string;
}
export interface MembersInterface {
  members?: MemberInterface[];
}

export interface TicketInterface {
  attachment: string[];
  backlog: string;
  comment: string[];
  createdAt: string;
  createdBy: string;
  description: string;
  epic: string;
  epicName: string;
  flag: string;
  label: number;
  link: string[];
  log: string[];
  member: string[];
  name: string;
  project: string;
  sprint: string;
  storyPoint: number;
  ticketId: string;
  type: string;
  updatedAt: string;
  __v: number;
  _id: string;
}
export interface TicketsInterface {
  data?: TicketInterface[];
}

export interface SprintInterface {
  createdAt: string;
  createdBy: string;
  epic: string[];
  name: string;
  project: string;
  startDate: string;
  status: string;
  ticket: TicketInterface[];
  updatedAt: string;
  __v: number;
  _id: string;
}

export interface ProjectInterface {
  projectId: {
    name: string;
    backlog: string;
    createdAt: string;
    createdBy: string;
    description: string;
    member: string[];
    projectType: string;
    updatedAt: string;
    __v: number;
    _id: string;
  };
}
export interface SprintIssueInterface {
  createdAt: string;
  createdBy: string;
  name: string;
  sprint: string;
  ticket: string[];
  updatedAt: string;
  __v: number;
  _id: string;
}

export interface EpicProps {
  createdAt: string;
  createdBy: string;
  name: string;
  sprint: string;
  ticket: any;
  updatedAt: string;
  __v: number;
  _id: string;
}

export interface errorCatchProps {
  code: string;
  config: string[] | any;
  message: string;
  name: string;
  request: any;
  response: any;
  stack: number;
}

export interface ReleaseProps {
  attachment: string[];
  date: string;
  description: string;
  title: string;
  url: string;
  _id: string;
}

export interface BreadcrumbTabProps {
  itemRender: string;
  ticketNumber?: number;
}

export interface IssueTableProps {
  act?: string;
}

export interface IssueProps {
  active?: string;
}

export interface ModalProps {
  showModal(value: boolean): void;
}
