import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import Head from "next/head";
import { useRouter } from "next/router";
import { notification } from "antd";
import Auth from "layouts/auth";
import usePost from "hooks/usePost";
import { VALIDATION_SCHEMA } from "utils/userValidation";

import Link from "next/link";

type NotificationType = "success" | "info" | "warning" | "error";

const Register = () => {
  const [show, setShow] = useState(false);

  const router = useRouter();

  const [api, contextHolder] = notification.useNotification();

  const openNotificationWithIcon = (
    type: NotificationType,
    message: string,
    description: string
  ) => {
    api[type]({
      message: message,
      description: description,
    });
  };

  const { mutateAsync } = usePost();

  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm({
    mode: "onSubmit",
    reValidateMode: "onChange",
    resolver: yupResolver(VALIDATION_SCHEMA),
    defaultValues: {
      name: "",
      email: "",
      password: "",
    },
  });

  const submitData = async (values: any) => {
    openNotificationWithIcon(
      "info",
      "Registration In Process",
      "User is being created, just wait for few seconds..."
    );
    try {
      const res = await mutateAsync({
        url: "/api/users/register",
        payload: values,
      });
      if (res.status == 201) {
        setTimeout(() => {
          router.push("/login");
        }, 1600);
      }
    } catch (error: any) {
      openNotificationWithIcon(
        "error",
        `${error?.response?.data?.errors?.[0]?.msg}`,
        "Something went wrong, Kindly try again later..."
      );
      return { error: error?.response?.data?.errorMessage };
    }
  };

  return (
    <Auth>
      {contextHolder}
      <Head>
        <title>Register</title>
      </Head>
      <div className="container mx-auto px-4 h-full">
        <div className="flex content-center items-center justify-center h-full">
          <div className="w-full lg:w-4/12 px-4">
            <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0">
              <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
                <div className="text-blueGray-400 text-center mb-3 font-bold pt-8">
                  <small>Sign up with credentials</small>
                </div>
                <form
                  className="flex flex-col"
                  onSubmit={handleSubmit(submitData)}
                >
                  <div className="relative w-full ">
                    <label
                      className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Name
                    </label>
                    <input
                      type="text"
                      className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                      placeholder="Name"
                      {...register("name")}
                    />
                  </div>
                  {errors.name ? (
                    <span className="text-rose-500 text-left text-sm mt-1 mb-2">
                      {errors?.name?.message}
                    </span>
                  ) : (
                    <span className="h-4"></span>
                  )}

                  <div className="relative w-full ">
                    <label
                      className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Email
                    </label>
                    <input
                      type="email"
                      className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                      placeholder="Email"
                      {...register("email")}
                    />
                  </div>
                  {errors.email ? (
                    <span className="text-rose-500 text-left text-sm mt-1 mb-2">
                      {errors?.email?.message}
                    </span>
                  ) : (
                    <span className="h-4"></span>
                  )}

                  <div className="relative w-full ">
                    <label
                      className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Password
                    </label>
                    <input
                      type={`${show ? "text" : "password"}`}
                      className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                      placeholder="Password"
                      {...register("password")}
                    />
                  </div>
                  {errors.password ? (
                    <span className="text-rose-500 text-left text-sm mt-1 mb-2">
                      {errors?.password?.message}
                    </span>
                  ) : (
                    <span className="h-4"></span>
                  )}

                  <div>
                    <label className="inline-flex items-center cursor-pointer">
                      <input
                        onClick={() => setShow(!show)}
                        id="customCheckLogin"
                        type="checkbox"
                        className="form-checkbox border-0 rounded text-blueGray-700 ml-1 w-5 h-5 ease-linear transition-all duration-150"
                      />
                      <span className="ml-2 text-sm font-semibold text-blueGray-600">
                        Show password
                      </span>
                    </label>
                  </div>

                  <div className="text-center mt-6">
                    <button
                      className="bg-blueGray-800 text-black active:bg-blueGray-600 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
                      type="submit"
                    >
                      Create Account
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div className="flex flex-wrap mt-6 relative">
              <div className="text-right">
                <Link href={"/login"} legacyBehavior>
                  <a className="text-blueGray-200">
                    <small>Already have a account ?</small>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Auth>
  );
};

export default Register;
