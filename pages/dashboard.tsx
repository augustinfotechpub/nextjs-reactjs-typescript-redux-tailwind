import Head from "next/head";
import Layout from "components/BoardItems/Layout";

const Dashboard = () => {
  return (
    <>
      <Head>
        <title>Dashboard</title>
      </Head>
      <Layout style={{ overflow: "hidden" }} />
    </>
  );
};

export default Dashboard;
