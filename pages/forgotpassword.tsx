import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import Head from "next/head";
import { useRouter } from "next/router";
import Link from "next/link";
import { notification } from "antd";
import Auth from "layouts/auth";
import usePost from "hooks/usePost";
import { VALIDATION_SCHEMA } from "utils/forgotpassword";

type NotificationType = "success" | "info" | "warning" | "error";

const Forgotpassword = () => {
  const router = useRouter();

  const { mutateAsync } = usePost();

  const [api, contextHolder] = notification.useNotification();

  const openNotificationWithIcon = (
    type: NotificationType,
    message: string,
    description: string
  ) => {
    api[type]({
      message: message,
      description: description,
      duration: 4,
    });
  };
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm({
    mode: "onSubmit",
    reValidateMode: "onChange",
    defaultValues: {
      email: "",
    },
    resolver: yupResolver(VALIDATION_SCHEMA),
  });

  const submitData = async (values: any) => {
    const email = values;
    try {
      openNotificationWithIcon(
        "info",
        "Email sending in progress",
        "Your email is getting sent in some second kindly wait thankyou.."
      );
      const response = await mutateAsync({
        url: "/api/users/sendPassword",
        payload: email,
      });
      if (response.status === 200) {
        openNotificationWithIcon(
          "success",
          "Email sent successfully",
          "You can now login with your new password sent on your email thankyou..."
        );
        setTimeout(() => {
          router.push("/login");
        }, 2500);
      }
    } catch (error: any) {
      openNotificationWithIcon(
        "error",
        "Some error has occurred",
        "Please try after some time or enter valid email adress thankyou..."
      );
      return { error: error?.response?.data?.errorMessage };
    }
  };
  return (
    <Auth>
      {contextHolder}
      <Head>
        <title>Forgot Password</title>
      </Head>
      <div className="container mx-auto px-4 h-full">
        <div className="flex content-center items-center justify-center h-full">
          <div className="w-full lg:w-4/12 px-4">
            <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0">
              <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
                <div className="text-blueGray-400 text-center mb-3 font-bold pt-8">
                  <small>Enter your email to get password over there...</small>
                </div>
                <form
                  className="flex flex-col gap-2"
                  onSubmit={handleSubmit(submitData)}
                >
                  <div className="relative w-full ">
                    <label
                      className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Email
                    </label>
                    <input
                      type="email"
                      className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                      placeholder="Email"
                      {...register("email")}
                    />
                  </div>
                  {errors.email ? (
                    <span className="text-rose-500 text-left text-sm mt-1 mb-2">
                      {errors?.email?.message}
                    </span>
                  ) : (
                    <span className="h-4"></span>
                  )}

                  <div className="text-center mt-6">
                    <button
                      className="bg-blueGray-800 text-white active:bg-blueGray-600 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
                      type="submit"
                    >
                      Send Password
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div className="flex flex-wrap mt-6 relative">
              <div className="text-right">
                <Link href={"/login"} legacyBehavior>
                  <a className="text-blueGray-200">
                    <small> Remember Password ?</small>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Auth>
  );
};

export default Forgotpassword;
