import React, { useContext, useState } from "react";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { useGoogleOneTapLogin } from "@react-oauth/google";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";

import { notification } from "antd";
import {
  GithubAuthProvider,
  signInWithPopup,
  GoogleAuthProvider,
} from "firebase/auth";
import { auth } from "config/firebase";
import Auth from "layouts/auth";
import usePost from "hooks/usePost";
import { VALIDATION_SCHEMA } from "utils/loginValidation";
import { LoaderContext } from "context/loader";
import Cookies from "js-cookie";

type NotificationType = "success" | "info" | "warning" | "error";

const Login = () => {
  const [show, setShow] = useState(false);
  const { setLoader } = useContext(LoaderContext);
  const router = useRouter();

  const { mutateAsync } = usePost();

  const [api, contextHolder] = notification.useNotification();

  const openNotificationWithIcon = (
    type: NotificationType,
    message: string,
    description: string
  ) => {
    api[type]({
      message: message,
      description: description,
    });
  };
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm({
    mode: "onSubmit",
    reValidateMode: "onChange",
    resolver: yupResolver(VALIDATION_SCHEMA),
    defaultValues: {
      email: "",
      password: "",
    },
  });

  const submitData = async (values: any) => {
    setLoader(true);
    try {
      openNotificationWithIcon(
        "info",
        "Login In Process",
        "We are taking you in, just wait for few seconds..."
      );
      const response = await mutateAsync({
        url: "/api/users/login",
        payload: values,
      });
      if (response?.data?.token) {
        localStorage.setItem("token", response?.data?.token);
        Cookies.set("loggedin", "true");
        router.push("/dashboard");
        setLoader(false);
      }
    } catch (error: any) {
      setLoader(false);
      openNotificationWithIcon(
        "error",
        `${error?.response?.data?.errors?.[0]?.msg}`,
        "Something went wrong, Kindly try again later..."
      );
      return { error: error?.response?.data?.errorMessage };
    }
  };

  // Google Handler function

  useGoogleOneTapLogin({
    onSuccess: async (credentialResponse:any) => {
      openNotificationWithIcon(
        "info",
        "Login In Process",
        "We are taking you in, just wait for few seconds..."
      );
      const accessToken = {
        googleAccessToken: credentialResponse?.credential,
      };
      try {
        const response = await mutateAsync({
          url: "/api/users/register",
          payload: accessToken,
        });
        if (response.status === 201) {
          localStorage.setItem("token", response?.data?.token);
          Cookies.set("loggedin", "true");
          router.push("/dashboard");
        }
      } catch (error: any) {
        return { error: error?.response?.data?.errorMessage };
      }
    },
    onError: () => {},
  });
  // github auth
  const githubAuth = new GithubAuthProvider();

  const githubSignup = async () => {
    signInWithPopup(auth, githubAuth).then(async (res) => {
      const payload = {
        githubAccess: {
          email: res?.user.email,
          name: res?.user.email?.split("@")[0],
          avatar: res?.user?.photoURL,
        },
      };
      try {
        openNotificationWithIcon(
          "info",
          "Login In Process",
          "We are taking you in, just wait for few seconds..."
        );
        const response = await mutateAsync({
          url: "/api/users/register",
          payload: payload,
        });
        if (response.status === 201) {
          localStorage.setItem("token", response?.data?.token);
          Cookies.set("loggedin", "true");
          router.push("/dashboard");
        }
      } catch (error: any) {
        return { error: error?.response?.data?.errorMessage };
      }
    });
  };

  const provider = new GoogleAuthProvider();

  const google = async () => {
    signInWithPopup(auth, provider).then(async (result: any) => {
      const accessToken = {
        googleAccessToken: result?.user?.accessToken,
      };
      try {
        const response = await mutateAsync({
          url: "/api/users/register",
          payload: accessToken,
        });
        if (response.status === 201) {
          localStorage.setItem("token", response?.data?.token);
          Cookies.set("loggedin", "true");
          router.push("/dashboard");
        }
      } catch (error: any) {
        return { error: error?.response?.data?.errorMessage };
      }
    });
  };
  return (
    <Auth>
      {contextHolder}
      <Head>
        <title>Login</title>
      </Head>
      <div className="container mx-auto px-4 h-full">
        <div className="flex content-center items-center justify-center h-full">
          <div className="w-full lg:w-4/12 px-4">
            <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0">
              <div className="rounded-t mb-0 px-6 py-6">
                <div className="text-center mb-3">
                  <h6 className="text-blueGray-500 text-sm font-bold">
                    Sign in with
                  </h6>
                </div>
                <div className="btn-wrapper text-center">
                  <button
                    className="bg-white active:bg-blueGray-50 text-blueGray-700 font-normal px-4 py-2 rounded outline-none focus:outline-none mr-2 mb-1 uppercase shadow hover:shadow-md inline-flex items-center font-bold text-xs ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => githubSignup()}
                  >
                    <img alt="..." className="w-5 mr-1" src="/img/github.svg" />
                    Github
                  </button>
                  <button
                    className="bg-white active:bg-blueGray-50 text-blueGray-700 font-normal px-4 py-2 rounded outline-none focus:outline-none mr-1 mb-1 uppercase shadow hover:shadow-md inline-flex items-center font-bold text-xs ease-linear transition-all duration-150"
                    type="button"
                    onClick={google}
                  >
                    <img alt="..." className="w-5 mr-1" src="/img/google.svg" />
                    Google
                  </button>
                </div>

                <hr className="mt-6 border-b-1 border-blueGray-300" />
              </div>
              <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
                <div className="text-blueGray-400 text-center mb-3 font-bold">
                  <small>Or sign in with credentials</small>
                </div>
                <form
                  className="flex flex-col gap-2"
                  onSubmit={handleSubmit(submitData)}
                >
                  <div className="relative w-full ">
                    <label
                      className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Email
                    </label>
                    <input
                      type="email"
                      className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                      placeholder="Email"
                      {...register("email")}
                    />
                  </div>
                  {errors.email ? (
                    <span className="text-rose-500 text-left text-sm">
                      {errors?.email?.message}
                    </span>
                  ) : (
                    <span className="h-4"></span>
                  )}
                  <div className="relative w-full ">
                    <label
                      className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Password
                    </label>
                    <input
                      type={`${show ? "text" : "password"}`}
                      className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                      placeholder="Password"
                      {...register("password")}
                    />
                  </div>
                  {errors.password ? (
                    <span className="text-rose-500 text-left text-sm">
                      {errors?.password?.message}
                    </span>
                  ) : (
                    <span className="h-4"></span>
                  )}
                  <div>
                    <label className="inline-flex items-center cursor-pointer">
                      <input
                        onClick={() => setShow(!show)}
                        id="customCheckLogin"
                        type="checkbox"
                        className="form-checkbox border-0 rounded text-blueGray-700 ml-1 w-5 h-5 ease-linear transition-all duration-150"
                      />
                      <span className="ml-2 text-sm font-semibold text-blueGray-600">
                        Show password
                      </span>
                    </label>
                  </div>

                  <div className="text-center mt-6">
                    <button
                      className="bg-blueGray-800 text-black active:bg-blueGray-600 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
                      type="submit"
                    >
                      Sign In
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div className="flex flex-wrap mt-6 relative">
              <div className="w-1/2">
                <Link href={"/forgotpassword"} legacyBehavior>
                  <a className="text-blueGray-200">
                    <small>Forgot password?</small>
                  </a>
                </Link>
              </div>
              <div className="w-1/2 text-right">
                <Link href={"/register"} legacyBehavior>
                  <a className="text-blueGray-200">
                    <small>Create new account</small>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Auth>
  );
};

export default Login;
