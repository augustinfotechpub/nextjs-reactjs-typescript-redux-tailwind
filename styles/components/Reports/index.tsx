import styled from "styled-components";

export const MainContainer = styled.div``;

export const BarContainer = styled.div`
  display: flex;
  padding-top: 20px;
  justify-content: space-evenly;

  img {
    padding: 20px;
    cursor: pointer;
    width: 33vw;
  }
`;
export const ChartContainer = styled.div`
  display: flex;
  justify-content: space-around;
  padding-top: 50px;
  height: 60vh;

  img {
    width: 30vw;
  }
`;
