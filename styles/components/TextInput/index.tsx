import styled from "styled-components";
export const Container = styled.div`
  position: relative;
  h3 {
    color: red;
    font-size: 12px;
    text-align: center;
    margin-top: 5px;
  }
  &.responsive {
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
    input {
      border: none;
      border-radius: 0.84vw !important;
      font-size: 1.1rem !important;
      height: 3.5rem !important;
      background-color: rgba(255, 255, 255, 0.3) !important;
      padding: 0.35rem 1.4rem !important;
      :focus {
        border: none;
        background-color: rgba(255, 255, 255, 0.7) !important;
      }
      :focus-visible {
        outline: none;
      }
    }
  }
  input {
    border: none;
    border-radius: 12px !important;
    font-size: 16px !important;
    height: 50px !important;
    background-color: rgba(255, 255, 255, 0.3) !important;
    padding: 5px 20px !important;
    width: 100% !important;
    :focus {
      border: none;
      background-color: rgba(255, 255, 255, 0.7) !important;
    }
  }
  .eye-icon {
    position: absolute;
    right: 15px;
    top: 21px;
  }
`;
