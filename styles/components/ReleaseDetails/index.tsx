import styled from "styled-components";

export const DetailedContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px;
`;
export const DetailedWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
export const ImageContainer = styled.div`
  img {
    height: 400px;
    width: 30vw;
  }
`;

export const Heading = styled.h1`
  font-size: 20px;
  font-weight: 700;
`;
export const LinkContainer = styled.div`
  font-size: 14px;
  font-weight: 400;
  color: blue;
`;
export const InnerContainer = styled.div`
  padding-top: 20px;
`;

export const DetailedContent = styled.p`
  font-size: 16px;
  font-weight: 500;
`;
