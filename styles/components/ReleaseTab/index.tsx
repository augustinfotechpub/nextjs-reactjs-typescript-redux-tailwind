import styled from "styled-components";

export const Container = styled.div`
  width: 85vw;
`;

export const InputTextareaContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 95px;

  textarea {
    border: 1px solid #d9d9d9;
  }

  textarea:focus {
    border: none;
  }
`;

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 68px;

  .ant-space-vertical,
  .ant-picker {
    width: 100%;
  }

  .ant-picker-input {
    padding: 3px 0;
  }

  .error .ant-space-item .ant-picker.css-dev-only-do-not-override-1hyej8k {
    border: 3px solid red !important;
  }

  .success .ant-space-item .ant-picker.css-dev-only-do-not-override-1hyej8k {
    border: 3px solid #3b82f6 !important;
  }

  .ant-picker-focused {
    border: 3px solid #3b82f6 !important;
  }
  .ant-picker.css-dev-only-do-not-override-1hyej8k:hover {
    border: 1px solid #d9d9d9 !important;
  }

  input {
    font-weight: 600;
    border: 1px solid #d9d9d9;
  }
  input:focus {
    border: none;
  }

  .ant-picker .ant-picker-input > input:hover {
    border: none;
  }
`;

export const FormWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 24vw;
  padding: 10px;
`;

export const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 10px;
  /* min-width: 450px; */
  width: 100%;
  min-height: 250px;
  background: #ffffff;
  border-radius: 12px;
  padding: 20px;
`;

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const FormHeader = styled.h1`
  font-size: 20px;
  font-weight: 600;
`;

export const CloseModalButton = styled.div`
  display: flex;
  justify-content: end;
  cursor: pointer;
`;

export const DateContent = styled.div`
  .ant-picker-dropdown-placement-bottomLeft,
  .ant-picker-header {
    width: 30vw !important;
  }
`;

export const TableWrapper = styled.div`
  padding-top: 20px;
  width: 100%;
  padding-right: 24px;
  .ant-table {
    margin-bottom: 20px;
    overflow-y: scroll;
    scroll-behavior: auto;
    max-height: 60vh;
    ::-webkit-scrollbar {
      display: none;
    }
  }
`;
export const ButtonWrapper = styled.div`
  display: flex;
  button {
    max-width: 200px;
    width: 100%;
    border-radius: 5px;
    padding: 12px;
    font-weight: 600;
    font-size: 16px;
    :focus {
      outline: none;
    }
  }
`;
export const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
export const Wrapper = styled.div``;
