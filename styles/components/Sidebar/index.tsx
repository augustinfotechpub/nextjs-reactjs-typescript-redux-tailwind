import styled from "styled-components";

export const TabContainer = styled.div`
  height: 100vh;
  .ant-tabs-nav {
    background-color: #fff;
  }
  .ant-tabs-nav-wrap {
    width: 144px;
  }
  .ant-tabs-ink-bar.ant-tabs-ink-bar-animated {
    background: #036aa7;
  }
  .ant-tabs-tab-active .ant-tabs-tab-btn p,
  .ant-tabs-tab-active .ant-tabs-tab-btn svg {
    color: #036aa7;
  }
  .ant-tabs-tabpane.ant-tabs-tabpane-active {
    overflow-x: scroll;
    ::-webkit-scrollbar {
      display: none;
    }
  }

  .ant-tabs-tab li:hover {
    color: #036aa7;
  }
  .ant-tabs-tab {
    padding: 0 24px !important;
  }
  .anticon svg {
    width: 16px;
    height: 16px;
  }
`;
