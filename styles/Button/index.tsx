import styled from "styled-components";

interface IStyledButton {
  variant?: "contained" | "text" | "outline";
}

const getButtonStyles = ({ variant }: IStyledButton) => {
  switch (variant) {
    case "contained":
      return `
      background: #036aa7;
      color: #ffffff;
      border: none;
      `;
    case "outline":
      return `
      background: none;
      color: #1D1D1D;
      border: 1px solid #036aa7;
      `;
    case "text":
      return `
      background: #F1F1F1;
      color: none;
      border: none;
      `;

    default:
      return `
      background: #036aa7;
      color: #ffffff;
      border: none;
  `;
  }
};

export const StyledButton = styled.button<IStyledButton>`
  width: 100%;
  cursor: pointer;
  padding: 10px;
  ${({ variant }) => getButtonStyles({ variant })};
  font-size: 20px;
  font-weight: 500;
  min-height: 24px;
  border-radius: 12px;
  &:disabled {
    opacity: 0.7;
  }
`;
