import React, { ReactElement, useContext, useState } from "react";
import { forwardRef } from "react";
import { useDispatch } from "react-redux";
import { MoreOutlined } from "@ant-design/icons";
import Modal from "react-modal";
import { Dropdown, notification, Space, Tooltip } from "antd";
import type { MenuProps } from "antd";
import Members from "components/Avatar";
import SprintModal from "views/MoveToSprintModal";
import TicketModal from "components/TicketModal";
import { fetchSprint } from "store/sprintSlice";
import { fetchBacklog } from "store/backlogSlice";
import { TicketInterface, TicketsInterface } from "interface";
import { LoaderContext } from "context/loader";
import usePost from "hooks/usePost";
import BugIcon from "assets/Svg/BugIcon";
import EpicIcon from "assets/Svg/EpicIcon";
import SubTaskIcon from "assets/Svg/SubTaskIcon";
import TaskIcon from "assets/Svg/TaskIcon";
import StoryIcon from "assets/Svg/StoryIcon";
import {
  TableWrapper,
  TableContainer,
  Box,
  IconContainer,
  TitleContainer,
  LabelContainer,
  MemberContainer,
  TicketContainer,
} from "styles/Backlog/BacklogTable";

export interface ITableData {
  id: number;
  icon: ReactElement | null;
  ticketId: string;
  priority: string;
  src: ReactElement | null;
}
type NotificationType = "success" | "info" | "warning" | "error";

const BacklogTable = forwardRef((data: TicketsInterface, ref: any) => {
  const [role, setRole] = useState(false);
  const [sprintModal, setSprintModal] = useState(false);
  const [ticketId, setticketId] = useState("");
  const [ticketDetails, setTicketDetails] =
    React.useState<TicketInterface | null>();
  const { mutateAsync } = usePost();
  const [api, contextHolder] = notification.useNotification();
  const { setLoader } = useContext(LoaderContext);
  const dispatch = useDispatch();

  const openNotificationWithIcon = (
    type: NotificationType,
    message: string,
    description: string
  ) => {
    api[type]({
      message: message,
      description: description,
      duration: 4,
    });
  };
  const labelChecker = (label: number) => {
    if (label == 1) {
      return "Critical";
    } else if (label == 2) {
      return "High";
    } else if (label == 3) {
      return "Medium";
    } else {
      return "Low";
    }
  };
  const iconChecker = (type: string) => {
    if (type == "epic") {
      return <EpicIcon />;
    } else if (type == "userStory") {
      return <StoryIcon />;
    } else if (type == "bug") {
      return <BugIcon />;
    } else if (type == "subTask") {
      return <SubTaskIcon />;
    } else {
      return <TaskIcon />;
    }
  };

  const moveSprint = () => {
    setSprintModal(true);
  };

  const deleteTicket = async () => {
    setLoader(true);
    const payload = {
      ticketId: ticketId,
      projectId: window.localStorage.getItem("projectId"),
    };
    try {
      openNotificationWithIcon(
        "info",
        "Deleting ticket",
        "Deleting ticket in Progress kindly wait for few seconds..."
      );
      const response = await mutateAsync({
        url: "/api/tickets/delete",
        payload: payload,
      });
      if (response.status === 200) {
        openNotificationWithIcon(
          "success",
          "Ticket Deleted Successfully",
          "Ticket deleted successfully thankyou..."
        );
        dispatch(fetchBacklog());
        dispatch(fetchSprint());
        setLoader(false);
      }
    } catch (error: any) {
      openNotificationWithIcon(
        "error",
        error?.response?.data?.msg,
        "Please try after some time thankyou..."
      );
      setLoader(false);
      return { error: error?.response?.data?.errorMessage };
    }
  };
  const items: MenuProps["items"] = [
    {
      key: "1",
      label: <a onClick={moveSprint}>Move to sprint</a>,
    },
    {
      key: "2",
      label: <a>Update</a>,
    },
    {
      key: "3",
      label: <a onClick={deleteTicket}>Delete</a>,
    },
  ];
  const dateConvertor = (date: string) => {
    const event = new Date(date);
    return event.toDateString();
  };

  return (
    <TableWrapper>
      {contextHolder}
      <TableContainer>
        {data &&
          data.data?.map((item: TicketInterface, index: number) => {
            return (
              <Box
                key={index}
                onClick={() => {
                  setTicketDetails(item);
                }}
              >
                <IconContainer>{iconChecker(item?.type)}</IconContainer>
                <TitleContainer>{item?.name}</TitleContainer>
                <Tooltip placement="top" title={"Ticket Id"} color={"#2db7f5"}>
                  <TicketContainer>{item?.ticketId}</TicketContainer>
                </Tooltip>
                <Tooltip placement="top" title={"created At"} color={"#2db7f5"}>
                  <TicketContainer className="date">
                    {dateConvertor(item?.createdAt)}
                  </TicketContainer>
                </Tooltip>
                <MemberContainer>
                  <Members />
                </MemberContainer>
                <LabelContainer className={`${labelChecker(item?.label)}`}>
                  {labelChecker(item?.label)}
                </LabelContainer>
                <Space direction="vertical">
                  <Space
                    wrap
                    onClick={(e) => {
                      e.stopPropagation();
                    }}
                  >
                    <Dropdown
                      menu={{ items }}
                      placement="bottomLeft"
                      trigger={["click"]}
                    >
                      <MoreOutlined
                        ref={ref}
                        onClick={() => setticketId(item?._id)}
                      />
                    </Dropdown>
                  </Space>
                </Space>
              </Box>
            );
          })}
      </TableContainer>
      <Modal
        isOpen={sprintModal}
        onRequestClose={() => setSprintModal(false)}
        ariaHideApp={false}
      >
        <SprintModal
          showModal={(value: boolean) => setSprintModal(value)}
          ticketId={ticketId}
        />
      </Modal>

      <Modal
        isOpen={role}
        onRequestClose={() => setRole(false)}
        ariaHideApp={false}
      >
        <TicketModal
          showModal={(value: boolean) => setRole(value)}
          detail={ticketDetails}
        />
      </Modal>
    </TableWrapper>
  );
});

export default BacklogTable;
