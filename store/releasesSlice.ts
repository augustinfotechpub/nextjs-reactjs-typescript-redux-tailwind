import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import type { RootState } from "store/store";
import axioPath from "services/axiosForStore";

export const STATUSES = Object.freeze({
  IDLE: "idle",
  ERROR: "error",
  LOADING: "loading",
});

interface releaseState {
  data: void[];
  status: string;
}

const initialState: releaseState = {
  data: [],
  status: STATUSES.IDLE,
};

const releaseSlice = createSlice({
  name: "releases",
  initialState,
  reducers: {
    setReleases(state, action) {
      state.data = action.payload;
    },
    setStatus(state, action) {
      state.status = action.payload;
    },
    resetreleaseState: (state, action) => {
      state.data = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchReleases.pending, (state, action) => {
        state.status = STATUSES.LOADING;
      })
      .addCase(fetchReleases.fulfilled, (state, action) => {
        state.data = action.payload;
        state.status = STATUSES.IDLE;
      })
      .addCase(fetchReleases.rejected, (state, action) => {
        state.status = STATUSES.ERROR;
      });
  },
});

export const { setReleases, setStatus, resetreleaseState } =
  releaseSlice.actions;

export const projects = (state: RootState) => state.releases;

export default releaseSlice.reducer;

export const fetchReleases = createAsyncThunk("projects/fetch", async () => {
  const token = window.localStorage.getItem("token");

  const payload = {
    projectId: window.localStorage.getItem("projectId"),
  };
  return axioPath
    .post("/api/releases/get", payload, {
      headers: {
        token: token,
      },
    })
    .then((response) => {
      const data = response.data;
      return data;
    })
    .catch((ex) => {
      if (typeof ex == "string") {
        return { ex: { message: ex } };
      }
      return { error: true, data: ex };
    });
});
