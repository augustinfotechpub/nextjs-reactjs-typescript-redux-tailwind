import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import type { RootState } from "store/store";
import axioPath from "services/axiosForStore";

export const STATUSES = Object.freeze({
  IDLE: "idle",
  ERROR: "error",
  LOADING: "loading",
});

interface UserState {
  data: void[];
  status: string;
}

const initialState: UserState = {
  data: [],
  status: STATUSES.IDLE,
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUser(state, action) {
      state.data = action.payload;
    },
    setStatus(state, action) {
      state.status = action.payload;
    },
    resetUserState: (state, action) => {
      state.data = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchUser.pending, (state, action) => {
        state.status = STATUSES.LOADING;
      })
      .addCase(fetchUser.fulfilled, (state, action) => {
        state.data = action.payload;
        state.status = STATUSES.IDLE;
      })
      .addCase(fetchUser.rejected, (state, action) => {
        state.status = STATUSES.ERROR;
      });
  },
});

export const { setUser, setStatus, resetUserState } = userSlice.actions;

export const user = (state: RootState) => state.user;

export default userSlice.reducer;

export const fetchUser = createAsyncThunk("user/fetch", async () => {
  let token = window.localStorage.getItem("token");

  return axioPath
    .get("/api/users/get", {
      headers: {
        token: token,
      },
    })
    .then((response) => {
      const data = response.data;
      return data;
    })
    .catch((ex) => {
      if (typeof ex == "string") {
        return { ex: { message: ex } };
      }
      return { error: true, data: ex };
    });
});
