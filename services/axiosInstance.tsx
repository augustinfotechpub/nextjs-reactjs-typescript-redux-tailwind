import axios from "axios";

const workForceInstance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BACKEND_BASEURL,
});

workForceInstance.interceptors.response.use(
  function (response:any) {
    return response;
  },
  function (error:any) {
    if (error?.response?.status === 401) {
      localStorage.clear();
      window.location.href = "/";
    }

    return Promise.reject(error);
  }
);

export default workForceInstance;
