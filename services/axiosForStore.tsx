import axios from "axios";
import { store } from "store/store";

const axioPath = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BACKEND_BASEURL,
  headers: {
    "Content-Type": "application/json",
  },
});

axioPath.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

axioPath.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.request.status === 401) {
      window.localStorage.removeItem("token");
    }
    return Promise.reject(error);
  }
);

export default axioPath;
